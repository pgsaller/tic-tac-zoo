let player = 1;
let intro = document.querySelector('#intro');
let score = document.querySelector('#winner');
let info = document.querySelector('#info');
let result = document.querySelector('#result');
let field = [
    [0,0,0],
    [0,0,0],
    [0,0,0]
];
let sceneEl = document.querySelector('a-scene');
let audioEl = document.querySelector('a-sound');
let animal = {
    name: 'lion',
    scale: {x: 0.2, y: 0.2, z: 0.2}
};

let results = [
    {
        name: 'lion',
        position: {x: 15, y: 2.4, z: -22.9},
        win: 0,
        remis: 0,
        loss: 0
    },
    {
        name: 'sealion',
        position: {x: 15, y: 2.4, z: -24.5},
        win: 0,
        remis: 0,
        loss: 0
    },
    {
        name: 'rabbit',
        position: {x: 15, y: 0.4, z: -24.5},
        win: 0,
        remis: 0,
        loss: 0
    },
    {
        name: 'monkey',
        position: {x: 15, y: 1.1, z: -24.5},
        win: 0,
        remis: 0,
        loss: 0
    },
    {
        name: 'lobster',
        position: {x: 15, y: 0.5, z: -22.8},
        win: 0,
        remis: 0,
        loss: 0
    },
    {
        name: 'pig',
        position: {x: 15, y: 1.75, z: -24.5},
        win: 0,
        remis: 0,
        loss: 0
    },
    {
        name: 'egret',
        position: {x: 15, y: 1.75, z: -22.9},
        win: 0,
        remis: 0,
        loss: 0
    },
    {
        name: 'cow',
        position: {x: 15, y: 1.05, z: -22.9},
        win: 0,
        remis: 0,
        loss: 0
    },
];

function clicked(box) {
    let pos = box.id.split(',');
    if (box.player !== -1 && box.player !== 1) {
        if (player === 1) {
            box.player = 1;
            player = 2;
            score.setAttribute('text', 'value:;');

            let obj = document.createElement('a-entity');
            obj.setAttribute('gltf-model', '#' + animal.name);
            obj.setAttribute('scale', animal.scale);
            obj.setAttribute('position',{x: box.getAttribute('position').x, y: box.getAttribute('position').y - 0.5, z: box.getAttribute('position').z});
            obj.setAttribute('id', 'obj' + box.id);
            box.setAttribute('visible', false);

            audioEl.setAttribute('src','#' + animal.name + '-audio');
            let audio = audioEl.components.sound;
            audio.playSound();

            sceneEl.appendChild(obj);
        } else {
            box.player = -1;
            player = 1;
            box.setAttribute('color', 'green');
        }
        field[pos[0]][pos[1]] = box.player;
        check();
    }
}

function computer() {
    let boxes = [
        {
            x: 0,
            y: 0,
            val: field[0][0]
        },
        {
            x: 1,
            y: 0,
            val: field[1][0]
        },
        {
            x: 1,
            y: 1,
            val: field[1][1]
        },
        {
            x: 1,
            y: 2,
            val: field[1][2]
        },
        {
            x: 2,
            y: 2,
            val: field[2][2]
        },
        {
            x: 2,
            y: 1,
            val: field[2][1]
        },
        {
            x: 0,
            y: 1,
            val: field[0][1]
        },
        {
            x: 0,
            y: 2,
            val: field[0][2]
        },
    ];

    boxes = boxes.filter(box => box.val == 0);
    let random = Math.floor(Math.random() * boxes.length);
    let box = boxes[random];
    let click = document.getElementById(box.x + ',' + box.y);
    setTimeout(() => {
        click.click();
        score.setAttribute('text', 'value: It is your turn ' + animal.name.toUpperCase());
    }, 500);
}

function check() {
    var p1row = 0;
    var p2row = 0;
    var p1col = 0;
    var p2col = 0;
    var clicks = 0;
    var win = undefined;

    for (var i = 0; i < 3; i++) {
        for (var j = 0; j < 3; j++) {
            if (field[i][j] == 1) {
                p1row += 1;
            }
            if (field[i][j] == -1) {
                p2row += 1;
            }
            if (field[j][i] == 1) {
                p1col += 1;
            }
            if (field[j][i] == -1) {
                p2col += 1;
            }
            if (field[i][j] != 0) {
                clicks += 1;
            }
        }

        if (p1row == 3 || p1col == 3 || field[0][0] + field[1][1] + field[2][2] == 3 || field[0][2] + field[1][1] + field[2][0] == 3) {
            console.log("p1 win");
            win = true;
            result.setAttribute('text', 'value: WIN; color:green');
        }
        if (p2row == 3 || p2col == 3 || field[0][0] + field[1][1] + field[2][2] == -3 || field[0][2] + field[1][1] + field[2][0] == -3) {
            console.log("p2 win");
            win = false;
            result.setAttribute('text', 'value: LOSE; color:red');
        } else {
            p1row = 0;
            p2row = 0;
            p1col = 0;
            p2col = 0;
        }
    }
    if (clicks === 9 && win == undefined) {
        console.log("draw");
        result.setAttribute('text', 'value: DRAW; color:black');
        createAnimal();
        setRecord('remis');
    } else if (win == true) {
        setRecord('win');
        createAnimal();
    } else if (win == false) {
        setRecord('loss');
        createAnimal();
    }

    if (player == 2 && win == undefined) {
        computer();
    }
}

function createAnimal() {
    let obj = document.createElement('a-entity');
    obj.setAttribute('gltf-model', '#' + animal.name);
    obj.setAttribute('scale', animal.scale);
    obj.setAttribute('position',{x: -8, y: 0.5, z: -16});
    obj.setAttribute('rotation',{x: 0, y: 90, z: 0});
    obj.setAttribute('id', 'win-animal');
    sceneEl.appendChild(obj);

    score.setAttribute("text", "value: Click " + animal.name.toUpperCase() + " to restart; color:black");

    obj.addEventListener('click', reset);
}

function reset() {
    for (var i = 0; i < 3; i++) {
        for (var j = 0; j < 3; j++) {
            let box = document.getElementById(`${i},${j}`);
            let obj = document.getElementById(`obj${i},${j}`);
            box.setAttribute('color','#AA0000');
            box.setAttribute('visible', true);
            box.setAttribute('opacity', 1);
            box.player = 0;
            if (obj) {
                obj.parentNode.removeChild(obj);
            }
        }
    }
    score.setAttribute('text','value: It is your turn ' + animal.name.toUpperCase());
    result.setAttribute('text', 'value:;');
    let animalEl = document.getElementById('win-animal');
    if (animalEl) {
        animalEl.parentNode.removeChild(animalEl);
    }
    player = 1;
    field = [
        [0,0,0],
        [0,0,0],
        [0,0,0]
    ];
}

function changeColor(color) {
    for (var i = 0; i < 3; i++) {
        for (var j = 0; j < 3; j++) {
            let box = document.getElementById(`${i},${j}`);
            if (box.player !== -1 && box.player !== 1) {
                box.setAttribute('color',color);
            }
        }
    }
}

function hover(box) {
    if (box.player !== 1 && box.player !== -1) {
        box.setAttribute('opacity', 0.8);
    }
}

function mouseleave(box) {
    if (box.player !== 1 && box.player !== -1) {
        box.setAttribute('opacity', 1);
    }
}

function startGame(name, scale) {
    let checkpoint = document.getElementById('checkpoint-game');
    checkpoint.click();
    animal = {name, scale};
    score.setAttribute('text', 'value: It is your turn ' + animal.name.toUpperCase() + '; color:black');
    info.setAttribute('visible', false);
    intro.setAttribute('visible', false);
}

function back() {
    let checkpoint = document.getElementById('checkpoint-start');
    checkpoint.click();
    score.setAttribute('text', 'value: Choose an animal to start the game');
    info.setAttribute('visible', true);
    intro.setAttribute('visible', true);
}

function goToRanking(rooster) {
    if (rooster) {
        audioEl.setAttribute('src','#rooster-audio');
        let audio = audioEl.components.sound;
        audio.playSound();
    }
    let checkpoint = document.getElementById('checkpoint-ranking');
    checkpoint.click();
    info.setAttribute('visible', false);
    intro.setAttribute('visible', false);
    reset();
    score.setAttribute("text", "value: Choose an animal to start the game; color:white");
}

function setRecord(result) {
    for (let r of results) {
        if (r.name == animal.name) {
            r[result] += 1;
            if (result == 'win') {
                let obj = document.createElement('a-entity');
                obj.setAttribute('gltf-model', '#trophy');
                obj.setAttribute('scale', {x: 1, y: 1, z: 1});
                obj.setAttribute('position',{x: (r.position.x - 1.2), y: (r.position.y - 1.6), z: (r.position.z + 0.9) + ((r.win - 1) * 0.1)});
                sceneEl.appendChild(obj);
            }
        }
    }
}